/*****************************************************************************
* | File      	:	DEV_Config.c
* | Author      :   Waveshare team
* | Function    :	Show SDcard BMP picto LCD 
* | Info        :
*   Provide the hardware underlying interface	 
*----------------
* |	This version:   V1.0
* | Date        :   2018-01-11
* | Info        :   Basic version
*
******************************************************************************/
#include "DEV_Config.h"
#include "stm32f4xx_hal_spi.h"
//#include "usart.h"
//#include "spi.h"
//#include "tim.h"
#include <errno.h>
#include <stdio.h>
#include <string.h>

extern SPI_HandleTypeDef hspi3;

/********************************************************************************
function:	System Init
note:
	Initialize the communication method
********************************************************************************/
uint8_t System_Init(void)
{
#if USE_SPI_4W
    printf("USE 4wire spi\r\n");
#elif USE_IIC
    printf("USE i2c\r\n");
#endif
    return 0;
}

void System_Exit(void)
{

}
/*
void PWM_SetValue(uint16_t value)
{		
////	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	
    TIM_OC_InitTypeDef sConfigOC;
	
    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = value;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2);
    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);  
}
*/
/*********************************************
function:	Hardware interface
note:
	SPI4W_Write_Byte(value) : 
		Register hardware SPI
*********************************************/	
uint8_t SPI4W_Write_Byte(uint8_t value)                                    
{   
//	static uint8_t en=0;
//	if(en==0) {
    __HAL_SPI_ENABLE(&hspi3);
//		en=1;
//	}
    SPI3->CR2 |= (1) << 12;

    while((SPI3->SR & (1 << 1)) == 0)
        ;

    *((__IO uint8_t *)(&SPI3->DR)) = value;

    while(SPI3->SR & (1 << 7)) ; //Wait for not busy

while((SPI3->SR & (1 << 0)) == 0) ; // Wait for the receiving area to be empty

   return *((__IO uint8_t *)(&SPI3->DR));
}

uint8_t SPI4W_Read_Byte(uint8_t value)                                    
{
	return SPI4W_Write_Byte(value);
}

/********************************************************************************
function:	Delay function
note:
	Driver_Delay_ms(xms) : Delay x ms
	Driver_Delay_us(xus) : Delay x us
********************************************************************************/
void Driver_Delay_ms(uint32_t xms)
{
    HAL_Delay(xms);
}

void Driver_Delay_us(uint32_t xus)
{
	int j;
    for(j=xus; j > 0; j--);
}
