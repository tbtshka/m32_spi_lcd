/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
typedef enum {
	eRecieve,
	eTransmit
} eRS485_Mode;
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BTN1_Pin GPIO_PIN_13
#define BTN1_GPIO_Port GPIOC
#define BTN2_Pin GPIO_PIN_14
#define BTN2_GPIO_Port GPIOC
#define BTN3_Pin GPIO_PIN_15
#define BTN3_GPIO_Port GPIOC
#define BTN4_Pin GPIO_PIN_0
#define BTN4_GPIO_Port GPIOC
#define BTN5_Pin GPIO_PIN_3
#define BTN5_GPIO_Port GPIOC
#define ADC1_IN0_VIN_Pin GPIO_PIN_0
#define ADC1_IN0_VIN_GPIO_Port GPIOA
#define ADC1_IN8_5V_Pin GPIO_PIN_0
#define ADC1_IN8_5V_GPIO_Port GPIOB
#define ADC1_IN9_3V3_Pin GPIO_PIN_1
#define ADC1_IN9_3V3_GPIO_Port GPIOB
#define RS485_DE_RE_N_Pin GPIO_PIN_2
#define RS485_DE_RE_N_GPIO_Port GPIOB
#define BTN6_Pin GPIO_PIN_15
#define BTN6_GPIO_Port GPIOE
#define BTN7_Pin GPIO_PIN_12
#define BTN7_GPIO_Port GPIOB
#define BTN8_Pin GPIO_PIN_13
#define BTN8_GPIO_Port GPIOB
#define BTN9_Pin GPIO_PIN_14
#define BTN9_GPIO_Port GPIOB
#define BTN10_Pin GPIO_PIN_15
#define BTN10_GPIO_Port GPIOB
#define PWR_5V_EN_Pin GPIO_PIN_10
#define PWR_5V_EN_GPIO_Port GPIOD
#define PWR_3V3_EN_Pin GPIO_PIN_14
#define PWR_3V3_EN_GPIO_Port GPIOD
#define SD_CARD_DET_Pin GPIO_PIN_8
#define SD_CARD_DET_GPIO_Port GPIOA
#define LCD_LED_CTRL_Pin GPIO_PIN_10
#define LCD_LED_CTRL_GPIO_Port GPIOA
#define LCD_CS_Pin GPIO_PIN_15
#define LCD_CS_GPIO_Port GPIOA
#define CFG1_Pin GPIO_PIN_0
#define CFG1_GPIO_Port GPIOD
#define CFG2_Pin GPIO_PIN_1
#define CFG2_GPIO_Port GPIOD
#define TOUCH_IRQ_Pin GPIO_PIN_3
#define TOUCH_IRQ_GPIO_Port GPIOD
#define CFG3_Pin GPIO_PIN_4
#define CFG3_GPIO_Port GPIOD
#define LCD_RESET_Pin GPIO_PIN_5
#define LCD_RESET_GPIO_Port GPIOD
#define LCD_DC_RS_Pin GPIO_PIN_7
#define LCD_DC_RS_GPIO_Port GPIOD
#define SPI2_NSS_TOUCH_Pin GPIO_PIN_9
#define SPI2_NSS_TOUCH_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */
	#define END_OF_BUFFER_BIG 256


	#define USART1_DR         0x40011004
	#define USART2_DR         0x40004404
	#define USART3_DR         0x40004804
	#define USART4_DR         0x40004C04
	#define USART6_DR         0x40011404
	
	#define SPI4_DR           0x4001340C
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
