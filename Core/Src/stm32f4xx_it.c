/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define UART_Package_TimeOut 200
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

volatile uint8_t UART2_RxBuffer[END_OF_BUFFER_BIG];
volatile uint8_t UART3_RxBuffer[END_OF_BUFFER_BIG];
volatile uint8_t UART6_RxBuffer[END_OF_BUFFER_BIG];

uint32_t  UART2_timer = 0,UART3_timer = 0,UART6_timer = 0;
uint8_t f_UART2_Package_ready=0, f_UART3_Package_ready=0, f_UART6_Package_ready=0;
uint16_t 	byte_counter2,byte_counter3,byte_counter6;

uint8_t SPI4_recieve[255];
uint8_t spi_byte_counter=0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
extern SPI_HandleTypeDef hspi4;
extern DMA_HandleTypeDef hdma_usart6_rx;
extern DMA_HandleTypeDef hdma_usart6_tx;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern UART_HandleTypeDef huart6;
/* USER CODE BEGIN EV */
void clear_UART_Buffer(uint8_t num) {
	uint16_t i=0;
switch (num)
  {
  	case 2:
			for(i=0;i<END_OF_BUFFER_BIG;i++)
				UART2_RxBuffer[i]=0;
  		break;
  	case 3:
			for(i=0;i<END_OF_BUFFER_BIG;i++)
				UART3_RxBuffer[i]=0;
  		break;
  	case 6:
			for(i=0;i<END_OF_BUFFER_BIG;i++)
				UART6_RxBuffer[i]=0;
  		break;
  	default:
  		break;
  }	
}
/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  HAL_RCC_NMI_IRQHandler();
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
  while (1)
  {
  }
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
			  printf("HARD FAULT AHAHAHHAHAHAHAHAHHA\r\n");
				HAL_Delay(1000);
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */
	UART2_timer++;
	UART3_timer++;
	UART6_timer++;
	if (UART2_timer==UART_Package_TimeOut) {
		f_UART2_Package_ready=1;
		UART_SHOW(2,UART2_RxBuffer,80);	
		clear_UART_Buffer(2);
		byte_counter2=0;
	}
	if (UART3_timer==UART_Package_TimeOut) {
		f_UART3_Package_ready=1;
		UART_SHOW(3,UART3_RxBuffer,240);	
		clear_UART_Buffer(3);
		byte_counter3=0;
	}
	if (UART6_timer==UART_Package_TimeOut) {
		f_UART6_Package_ready=1;
		UART_SHOW(6,UART6_RxBuffer,160);	
		clear_UART_Buffer(6);
		byte_counter6=0;
	}
  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles USART2 global interrupt.
  */
void USART2_IRQHandler(void)
{
  /* USER CODE BEGIN USART2_IRQn 0 */
uint16_t i=0;
	if (((*(uint32_t *)(USART2_BASE) & USART_SR_RXNE) != RESET)) {
		UART2_timer = 0;
		UART2_RxBuffer[byte_counter2] = *(uint8_t *)(USART2_DR);
		byte_counter2++;
		if (byte_counter2 == END_OF_BUFFER_BIG) {
			//Errors[eUART3_TooManyBytes]++;
			byte_counter2=0;
			//printf("_____________________END OF BUFFER_____________________");
			for(i = 0 ; i < END_OF_BUFFER_BIG ; i++) {
				UART2_RxBuffer[i] = 0;
			}	
		}
	}
  /* USER CODE END USART2_IRQn 0 */
  HAL_UART_IRQHandler(&huart2);
  /* USER CODE BEGIN USART2_IRQn 1 */

  /* USER CODE END USART2_IRQn 1 */
}

/**
  * @brief This function handles USART3 global interrupt.
  */
void USART3_IRQHandler(void)
{
  /* USER CODE BEGIN USART3_IRQn 0 */
uint16_t i=0;
	if (((*(uint32_t *)(USART3_BASE) & USART_SR_RXNE) != RESET)) {
		UART3_timer = 0;
		UART3_RxBuffer[byte_counter3] = *(uint8_t *)(USART3_DR);
		byte_counter3++;
		if (byte_counter3 == END_OF_BUFFER_BIG) {
			//Errors[eUART3_TooManyBytes]++;
			byte_counter3=0;
			//printf("_____________________END OF BUFFER_____________________");
			for(i = 0 ; i < END_OF_BUFFER_BIG ; i++) {
				UART3_RxBuffer[i] = 0;
			}	
		}
	}
  /* USER CODE END USART3_IRQn 0 */
  HAL_UART_IRQHandler(&huart3);
  /* USER CODE BEGIN USART3_IRQn 1 */

  /* USER CODE END USART3_IRQn 1 */
}

/**
  * @brief This function handles DMA2 stream1 global interrupt.
  */
void DMA2_Stream1_IRQHandler(void)
{
  /* USER CODE BEGIN DMA2_Stream1_IRQn 0 */

  /* USER CODE END DMA2_Stream1_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart6_rx);
  /* USER CODE BEGIN DMA2_Stream1_IRQn 1 */

  /* USER CODE END DMA2_Stream1_IRQn 1 */
}

/**
  * @brief This function handles USB On The Go FS global interrupt.
  */
void OTG_FS_IRQHandler(void)
{
  /* USER CODE BEGIN OTG_FS_IRQn 0 */

  /* USER CODE END OTG_FS_IRQn 0 */
  HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
  /* USER CODE BEGIN OTG_FS_IRQn 1 */

  /* USER CODE END OTG_FS_IRQn 1 */
}

/**
  * @brief This function handles DMA2 stream6 global interrupt.
  */
void DMA2_Stream6_IRQHandler(void)
{
  /* USER CODE BEGIN DMA2_Stream6_IRQn 0 */

  /* USER CODE END DMA2_Stream6_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart6_tx);
  /* USER CODE BEGIN DMA2_Stream6_IRQn 1 */

  /* USER CODE END DMA2_Stream6_IRQn 1 */
}

/**
  * @brief This function handles USART6 global interrupt.
  */
void USART6_IRQHandler(void)
{
  /* USER CODE BEGIN USART6_IRQn 0 */
uint16_t i=0;
	if (((*(uint32_t *)(USART6_BASE) & USART_SR_RXNE) != RESET)) {
		UART6_timer = 0;
		UART6_RxBuffer[byte_counter6] = *(uint8_t *)(USART6_DR);
		byte_counter6++;
		if (byte_counter6 == END_OF_BUFFER_BIG) {
			//Errors[eUART3_TooManyBytes]++;
			byte_counter6=0;
			//printf("_____________________END OF BUFFER_____________________");
			for(i = 0 ; i < END_OF_BUFFER_BIG ; i++) {
				UART6_RxBuffer[i] = 0;
			}	
		}
	}
  /* USER CODE END USART6_IRQn 0 */
  HAL_UART_IRQHandler(&huart6);
  /* USER CODE BEGIN USART6_IRQn 1 */

  /* USER CODE END USART6_IRQn 1 */
}

/**
  * @brief This function handles SPI4 global interrupt.
  */
void SPI4_IRQHandler(void)
{
  /* USER CODE BEGIN SPI4_IRQn 0 */
	if((SPI4->SR & (SPI_FLAG_RXNE)) == (SPI_SR_RXNE)){
		SPI4_recieve[spi_byte_counter] = *(uint16_t *)(SPI4_DR);
		spi_byte_counter++;
		if(spi_byte_counter==255)
		{
			spi_byte_counter=0;
			for(uint8_t i=0;i<255;i++) {
				SPI4_recieve[i]=0;
			}			
		}
	}
  /* USER CODE END SPI4_IRQn 0 */
  HAL_SPI_IRQHandler(&hspi4);
  /* USER CODE BEGIN SPI4_IRQn 1 */

  /* USER CODE END SPI4_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
